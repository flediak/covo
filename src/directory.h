// Kai Hoffmann
// Institute for Computational Science (ICS)
// University of Zurich
// 2019

#include <string>
#include <vector>

bool directory_exists(const short verbose, const std::string pathname);

void make_directory(const short verbose, const std::string pathname);

